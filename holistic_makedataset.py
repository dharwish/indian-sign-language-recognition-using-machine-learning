import mediapipe as mp
import cv2
import ImageAugmentation
import argparse
import csv
import os
from mediapipe.framework.formats import landmark_pb2
combined_op = list()
mp_drawing = mp.solutions.drawing_utils
mp_holistic = mp.solutions.holistic
class handDetection():

    def __init__(self, *args):
        super().__init__(*args)

    def decode_landmarks(self, landmarks: list, flatten: bool = True) -> dict:
        decoded_output = []
        keys = ['1', '2', '3', '4', '5', '6', '7', '8',
                '9', '10', '11', '12', '13', '14', '15', '16', '17',
                '18', '19', '20', '21']
        for i, key in enumerate(keys):
            data_point = landmarks[i]
            if flatten:
                x = data_point.x
                y = data_point.y
                decoded_output.append(x)
                decoded_output.append(y)
        return(decoded_output)

    def generate_landmark(self, results):

        landmark_output = []
        two_hand_null_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        temp_landmark_op = list()
        if results.left_hand_landmarks or results.right_hand_landmarks:
            if results.left_hand_landmarks:
                # for hand_landmarks in results.left_hand_landmarks:
                    temp_landmark_op = self.decode_landmarks(results.left_hand_landmarks.landmark)
            else:
                for y in range(len(two_hand_null_data)):
                    temp_landmark_op.append(two_hand_null_data[y])
            if results.right_hand_landmarks:
                    temp_landmark_op = temp_landmark_op + self.decode_landmarks(results.right_hand_landmarks.landmark)
            else:
                for y in range(len(two_hand_null_data)):
                    temp_landmark_op.append(two_hand_null_data[y])

            # print("HAND :", len(temp_landmark_op))
            return(temp_landmark_op)
        else:
            return([0])

class poseDetection():

    def __init__(self, *args):
        super().__init__(*args)

    def decode_landmarks(self, landmarks: list, flatten: bool = True) -> dict:
        decoded_output = []
        keys = ['1', '2', '3', '4', '5', '6', '7', '8',
                '9', '10', '11', '1repo ', '13', '14', '15', '16', '17', ]
        for i, key in enumerate(keys):
            data_point = landmarks.landmark[i]
            if flatten:
                x = data_point.x
                y = data_point.y
                decoded_output.append(x)
                decoded_output.append(y)
        return(decoded_output)

    def generate_landmark(self, results):
        landmark_output = []
        temp_landmark_op = list()
        if results.pose_landmarks:
            landmark_subset = landmark_pb2.NormalizedLandmarkList(
                landmark=[
                    results.pose_landmarks.landmark[0],
                    results.pose_landmarks.landmark[1],
                    results.pose_landmarks.landmark[2],
                    results.pose_landmarks.landmark[3],
                    results.pose_landmarks.landmark[4],
                    results.pose_landmarks.landmark[5],
                    results.pose_landmarks.landmark[6],
                    results.pose_landmarks.landmark[7],
                    results.pose_landmarks.landmark[8],
                    results.pose_landmarks.landmark[9],
                    results.pose_landmarks.landmark[10],
                    results.pose_landmarks.landmark[11],
                    results.pose_landmarks.landmark[12],
                    results.pose_landmarks.landmark[13],
                    results.pose_landmarks.landmark[14],
                    results.pose_landmarks.landmark[15],
                    results.pose_landmarks.landmark[16],
                ])
            temp_landmark_op = self.decode_landmarks(landmark_subset)
            # print("POSE :", len(temp_landmark_op))
            return(temp_landmark_op)
        else:
            return([0])
# handtemp_output = "abc"
def combined_landmark(results):
    pose = poseDetection()
    hand = handDetection()
    pose_output= list()
    hand_output= list()
    pose_output = pose.generate_landmark(results)
    hand_output = hand.generate_landmark(results)
    # print("Pose : ",pose_output)
    # print("Hand : ",hand_output)
    combined_landmark = list()
    # print(temp_hand_output)
    if pose_output != [0]:
        global posetemp_output
        posetemp_output = pose_output
        combined_landmark = combined_landmark + pose_output
    else:
        combined_landmark = combined_landmark + posetemp_output
    if hand_output != [0]:
        global handtemp_output
        handtemp_output = hand_output 
        combined_landmark = combined_landmark + hand_output
    else:
        combined_landmark = combined_landmark + handtemp_output
    # print("temp:",len(temp_pose_output),len(temp_hand_output))
    # print(combined_landmark)
    return(combined_landmark)

def write_to_csv(data):
    with open("ges_4_blured1.csv", "a") as fp:
        wr = csv.writer(fp)
        wr.writerow(data)
    return 1

def normal_video_process(input_path, ges_count, which_aug):
    aug = ImageAugmentation.VideoAugmentation()
    cap = cv2.VideoCapture(input_path)
    video_landmark_output = []
    max_frame = 60
    frame_count = 0
    print("AUG :", which_aug)
    with mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
        while cap.isOpened():

            # print("frame counter :", frame_count)
            if frame_count == max_frame:
                break
            ret, frame = cap.read()
            if not ret:
                break
            if which_aug == 0:
                image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            elif which_aug == 1:
                image = aug.GaussianBlur(frame, 35, 35)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            elif which_aug == 2:
                image = aug.InvertColor(frame)
            elif which_aug == 3:
                image = aug.RandomRotation(frame,15)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            elif which_aug == 4:
                image = aug.CenterCrop(frame,1280,900)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            elif which_aug == 5:
                image = aug.SaltAndPepper(frame)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            else:
                print("Error")
                exit
            
            # image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            results = holistic.process(image)

            # mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS)

            # mp_drawing.draw_landmarks(image, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS)

            # mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS)
                        
            # cv2.imshow('Raw Webcam Feed', image)

            # if cv2.waitKey(50) & 0xFF == ord('q'):
            #     break

            output = list()
            output = combined_landmark(results)
            combined_op = output
            for x in range(len(output)):
                video_landmark_output.append(output[x])
            frame_count = frame_count + 1
            # print("OUT: ",len(output))
        print("total",len(video_landmark_output))
        cap.release()
        cv2.destroyAllWindows()
        while frame_count < max_frame:
            for x in range(len(combined_op)):
                video_landmark_output.append(combined_op[x])  
            frame_count = frame_count + 1
            # print("No video part")
            # print("OUT: ",len(combined_op))
            # print("total",len(video_landmark_output))
            # print(frame_count)
        video_landmark_output.append(ges_count)
        print(len(video_landmark_output))
        if(len(video_landmark_output) != 7081):
            print("Error", input_path, ges_count)
        wrt = write_to_csv(video_landmark_output)
    return 1


parser = argparse.ArgumentParser(
    description='Demonstration of landmarks localization.')
parser.add_argument('--from_dir', type=str,
                    help='Path to a directory of videos')
args = parser.parse_args()
if args.from_dir is not None:
    directories = os.listdir(args.from_dir)
    ges_count = 1
    print(directories)
    for dir in directories:
        print("Ges:",ges_count,dir)
        videos = os.listdir(args.from_dir + "/" + dir)
        print(videos)
        # for index , video in zip(range(8), videos):
        video_count = 1
        for video in videos:
            print("Video Count :", video_count, video)
            input_path = os.path.join(args.from_dir, dir, video)
            normal_video_process(input_path, ges_count, 0)
            normal_video_process(input_path, ges_count, 1)
            normal_video_process(input_path, ges_count, 2)
            normal_video_process(input_path, ges_count, 3)
            normal_video_process(input_path, ges_count, 4)
            normal_video_process(input_path, ges_count, 5)
            video_count = video_count + 1
        ges_count = ges_count + 1
