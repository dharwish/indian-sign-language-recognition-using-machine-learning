import ImageAugmentation
import mediapipe as mp
import cv2
from multiprocessing.pool import Pool
input_path = "1.mp4"
cap = cv2.VideoCapture(input_path)
while cap.isOpened():
    success, image = cap.read()
    if not success:
        break
    a = 1
    aug = ImageAugmentation.VideoAugmentation()
    hand_image = aug.SaltAndPepper(image)
    pose_image = aug.SaltAndPepper(image)
    cv2.imshow("image", image)
    cv2.waitKey(1000)
    cv2.destroyAllWindows()
    print("1frame")
    mp_drawing = mp.solutions.drawing_utils
    mp_hands = mp.solutions.hands
    hands = mp_hands.Hands(min_detection_confidence=0.5,
        min_tracking_confidence=0.5, max_num_hands=2)
    hand_image.flags.writeable = False
    results = hands.process(hand_image)
    # image.flags.writeable = True
    # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    if results.multi_hand_landmarks:
        print("Hand ind")
        for hand_landmarks in results.multi_hand_landmarks:
            mp_drawing.draw_landmarks(
                hand_image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
        cv2.imshow("image", hand_image)
        cv2.waitKey(1000)
        cv2.destroyAllWindows()
    mp_pose_drawing = mp.solutions.drawing_utils
    mp_pose = mp.solutions.pose
    pose = mp_pose.Pose(min_detection_confidence=0.5,
        min_tracking_confidence=0.5)
    results_pose = pose.process(pose_image)
    if results_pose.pose_landmarks:
        print("Pose Ind")
        mp_pose_drawing.draw_landmarks(
                pose_image, results_pose.pose_landmarks, mp_pose.POSE_CONNECTIONS)
        cv2.imshow("image", pose_image)
        cv2.waitKey(1000)
        cv2.destroyAllWindows()

hands.close()
cap.release()