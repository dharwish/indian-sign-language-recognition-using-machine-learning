import mediapipe as mp
import ImageAugmentation
import cv2
import ImageAugmentation
import argparse
import csv
import os
from mediapipe.framework.formats import landmark_pb2
combined_op = list()

class handDetection():

    def __init__(self, *args):
        super().__init__(*args)

    def decode_landmarks(self, landmarks: list, flatten: bool = True) -> dict:
        decoded_output = []
        keys = ['1', '2', '3', '4', '5', '6', '7', '8',
                '9', '10', '11', '12', '13', '14', '15', '16', '17',
                '18', '19', '20', '21']
        for i, key in enumerate(keys):
            data_point = landmarks[i]
            if flatten:
                x = data_point.x
                y = data_point.y
                decoded_output.append(x)
                decoded_output.append(y)
        return(decoded_output)

    def generate_landmark(self, image):
        print("CAP HAND: ",type(image))
        hand_img = image
        cv2.imshow("image", hand_img)
        cv2.waitKey(500)
        cv2.destroyAllWindows()
        hand_img.flags.writeable = True
        cv2.imshow("image", hand_img)
        cv2.waitKey(500)
        cv2.destroyAllWindows()
        landmark_output = []
        mp_hand_drawing = mp.solutions.drawing_utils
        mp_hands = mp.solutions.hands
        hands = mp_hands.Hands(min_detection_confidence=0.5,
                               min_tracking_confidence=0.1, max_num_hands=2)
        hand_img.flags.writeable = False
        results = hands.process(hand_img)
        two_hand_null_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        temp_landmark_op = list()
        yes_result = 0
        if results.multi_hand_landmarks:
            yes_result = 1
            num_hands_detected = len(results.multi_hand_landmarks)
            if(num_hands_detected == 1):
                for hand_landmarks in results.multi_hand_landmarks:
                    temp_landmark_op = self.decode_landmarks(
                        hand_landmarks.landmark)
                    for y in range(len(two_hand_null_data)):
                        temp_landmark_op.append(two_hand_null_data[y])
            elif(num_hands_detected == 2):
                for hand_landmarks in results.multi_hand_landmarks:
                    temp_landmark_op = temp_landmark_op + \
                        self.decode_landmarks(hand_landmarks.landmark)
            for hand_landmarks in results.multi_hand_landmarks:
                mp_hand_drawing.draw_landmarks(
                    hand_img, hand_landmarks, mp_hands.HAND_CONNECTIONS)
            print("HAND :", len(temp_landmark_op))
            cv2.imshow("image", hand_img)
            cv2.waitKey(500)
            cv2.destroyAllWindows()

            hands.close()
            return(temp_landmark_op)


        else:
            # print("NO HAND")
            hands.close()
            return([0])



class poseDetection():

    def __init__(self, *args):
        super().__init__(*args)

    def decode_landmarks(self, landmarks: list, flatten: bool = True) -> dict:
        decoded_output = []
        keys = ['1', '2', '3', '4', '5', '6', '7', '8',
                '9', '10', '11', '12', '13', '14', '15', '16', '17', ]
        for i, key in enumerate(keys):
            data_point = landmarks.landmark[i]
            if flatten:
                x = data_point.x
                y = data_point.y
                decoded_output.append(x)
                decoded_output.append(y)
        return(decoded_output)

    def generate_landmark(self, image):
        print("CAP PoSE: ",type(image))
        pose_img = image
        pose_img.flags.writeable = True
        cv2.imshow("image", pose_img)
        cv2.waitKey(500)
        cv2.destroyAllWindows()
        landmark_output = []
        mp_pose_drawing = mp.solutions.drawing_utils
        mp_pose = mp.solutions.pose
        pose = mp_pose.Pose(min_detection_confidence=0.5,
                            min_tracking_confidence=1)
        pose_img.flags.writeable = False
        results = pose.process(pose_img)
        # print(results.pose_landmarks)
        temp_landmark_op = list()
        if results.pose_landmarks:
            # for pose_landmarks in results.pose_landmarks:
            landmark_subset = landmark_pb2.NormalizedLandmarkList(
                landmark=[
                    results.pose_landmarks.landmark[0],
                    results.pose_landmarks.landmark[1],
                    results.pose_landmarks.landmark[2],
                    results.pose_landmarks.landmark[3],
                    results.pose_landmarks.landmark[4],
                    results.pose_landmarks.landmark[5],
                    results.pose_landmarks.landmark[6],
                    results.pose_landmarks.landmark[7],
                    results.pose_landmarks.landmark[8],
                    results.pose_landmarks.landmark[9],
                    results.pose_landmarks.landmark[10],
                    results.pose_landmarks.landmark[11],
                    results.pose_landmarks.landmark[12],
                    results.pose_landmarks.landmark[13],
                    results.pose_landmarks.landmark[14],
                    results.pose_landmarks.landmark[15],
                    results.pose_landmarks.landmark[16],
                ])
            mp_pose_drawing.draw_landmarks(
                pose_img, landmark_subset)
            temp_landmark_op = self.decode_landmarks(landmark_subset)

            cv2.imshow("image", pose_img)
            cv2.waitKey(500)
            cv2.destroyAllWindows()
            # print(results.pose_landmarks)
            # print(temp_landmark_op)
            # print("POSE", len(temp_landmark_op))
            pose.close()
            return(temp_landmark_op)
        else:
            # print("NO POSE")
            pose.close()
            return([0])
# handtemp_output = "abc"
def combined_landmark(image):
    pose = poseDetection()
    hand = handDetection()
    pose_output= list()
    hand_output= list()
    # pose_output = pose.generate_landmark(image)
    hand_output = hand.generate_landmark(image)
    # print("Pose : ",pose_output)
    # print("Hand : ",hand_output)
    combined_landmark = list()
    # print(temp_hand_output)
    # if pose_output != [0]:
    #     global posetemp_output
    #     posetemp_output = pose_output
    #     combined_landmark = combined_landmark + pose_output
    # else:
    #     combined_landmark = combined_landmark + posetemp_output
    if hand_output != [0]:
        global handtemp_output
        handtemp_output = hand_output 
        combined_landmark = combined_landmark + hand_output
    else:
        combined_landmark = combined_landmark + handtemp_output
    # print("temp:",len(temp_pose_output),len(temp_hand_output))
    print(combined_landmark)
    return(combined_landmark)

def write_to_csv(data):
    with open("ges_4_blured.csv", "a") as fp:
        wr = csv.writer(fp)
        wr.writerow(data)
    return 1

def normal_video_process(input_path, ges_count, which_aug):
    aug = ImageAugmentation.VideoAugmentation()
    cap = cv2.VideoCapture(input_path)
    video_landmark_output = []
    max_frame = 60
    frame_count = 0
    while cap.isOpened():
        # print("frame counter :", frame_count)
        
        if frame_count == max_frame:
            break
        success, image = cap.read()
        if not success:
            break
        print("CAP: ",type(image))
        if which_aug == 0:
            continue
        elif which_aug == 1:
            image = image = cv2.cvtColor(aug.GaussianBlur(image, 35, 35), cv2.COLOR_RGB2BGR)
        elif which_aug == 2:
            image = aug.InvertColor(image)
        elif which_aug == 3:
            image = cv2.cvtColor(aug.RandomRotation(image,15), cv2.COLOR_RGB2BGR)
        elif which_aug == 4:
            image = aug.CenterCrop(image,1280,900)
        elif which_aug == 5:
            image = aug.SaltAndPepper(image)

        cv2.imshow("image", image)
        cv2.waitKey(500)
        cv2.destroyAllWindows()
        output = list()
        output = combined_landmark(image)
        combined_op = output
        for x in range(len(output)):
            video_landmark_output.append(output[x])
        # output.append(ges_count)
        # print("OUT: ",len(output))
        # print("total",len(video_landmark_output))
        frame_count = frame_count + 1
    cap.release()
    while frame_count < max_frame:
        for x in range(len(combined_op)):
            video_landmark_output.append(combined_op[x])  
        frame_count = frame_count + 1
        # print("No video part")
        # print("OUT: ",len(combined_op))
        # print("total",len(video_landmark_output))
        # print(frame_count)
    video_landmark_output.append(ges_count)
    print(len(video_landmark_output))
    wrt = write_to_csv(video_landmark_output)
    return 1


parser = argparse.ArgumentParser(
    description='Demonstration of landmarks localization.')
parser.add_argument('--from_dir', type=str,
                    help='Path to a directory of videos')
args = parser.parse_args()
if args.from_dir is not None:
    directories = os.listdir(args.from_dir)
    ges_count = 1
    print(directories)
    for dir in directories:
        print(dir)
        videos = os.listdir(args.from_dir + "/" + dir)
        print(videos)
        # for index , video in zip(range(8), videos):
        video_count = 1
        for video in videos:
            print("Video Count :", video_count, video)
            input_path = os.path.join(args.from_dir, dir, video)
            normal_video_process(input_path, ges_count, 5)
            video_count = video_count + 1
        ges_count = ges_count + 1
# cap = cv2.VideoCapture(input_path)
# while cap.isOpened():
#     success, image = cap.read()
#     if not success:
#         break
#     print(combined_landmark(image))
#     # cv2.imshow("image", image)
#     # cv2.waitKey(300)
#     # cv2.destroyAllWindows()
#     # pose = poseDetection()
#     # hand = handDetection()
#     # img = pose.generate_landmark(image)
#     # cv2.imshow("image", hand.generate_landmark(image))
#     # cv2.waitKey(500)
#     # cv2.destroyAllWindows()
    # break
# print("End")
# cap.release()
