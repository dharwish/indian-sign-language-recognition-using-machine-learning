import argparse
import random
from imutils.video import count_frames
import os
import skimage as sk
import json
import string
import pandas as pd
import mediapipe as mp
import cv2
import time
import sys
import csv
from skimage import transform
from skimage import util
from skimage import data
from skimage import filters
from skimage import color
from skimage import exposure
from skimage.transform import rotate
from skimage.transform import warp
from skimage.transform import ProjectiveTransform
from skimage.transform import AffineTransform
import numpy as np
import skvideo.io  
from vidaug import augmentors as va

possible_video_format = ['.mp4','.MOV','.MP4','.mov']
format_tuple = tuple(possible_video_format)
mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands
failed_vid_count = 0
failed_vid = []
ges = 1
# ges_count = ['24','98','100','137','159','206','225','237','244','246','255']
info_file = open("info_4_blured.txt","w") 
info_file.write("Last Appended Value | Output CSV File | Number Of videos \n") 
def decode_landmarks(landmarks: list, flatten: bool = True) -> dict:
    decoded_output = []
    keys = ['1', '2', '3', '4', '5', '6', '7', '8',
            '9', '10', '11', '12', '13', '14', '15', '16', '17',
            '18', '19', '20', '21']
    for i, key in enumerate(keys):
        data_point = landmarks[i]
        if flatten:
            x = data_point.x
            y = data_point.y
            decoded_output.append(x)
            decoded_output.append(y)
    return(decoded_output)


def random_noise_v(image):
    from skimage.util import random_noise
    image_with_random_noise = random_noise(image)
    return image_with_random_noise

def salt_pepper(image):
    img = image
    img_shape = img.shape
    noise = np.random.randint(5, size=img_shape)
    img = np.where(noise == 0, 255, img)
    img = np.where(noise == 0, 0, img)
    return img
def piecewise_affine(image):
    from skimage import transform
    from skimage.transform import rotate, PiecewiseAffineTransform
    from skimage.util import random_noise
    from skimage.filters import gaussian
    from scipy import ndimage
    rows, cols = image.shape[0], image.shape[1]
    src_cols = np.linspace(0, cols, 20)
    src_rows = np.linspace(0, rows, 10)
    src_rows, src_cols = np.meshgrid(src_rows, src_cols)
    src = np.dstack([src_cols.flat, src_rows.flat])[0]
    # add sinusoidal oscillation to row coordinates
    dst_rows = src[:, 1] - np.sin(np.linspace(0, 3 * np.pi, src.shape[0])) * 50
    dst_cols = src[:, 0]
    dst_rows *= 1.5
    dst_rows -= 1.5 * 50
    dst = np.vstack([dst_cols, dst_rows]).T
    tform = PiecewiseAffineTransform()
    tform.estimate(src, dst)
    out_rows = image.shape[0] - 1.5 * 50
    out_cols = cols
    out = warp(image, tform, output_shape=(out_rows, out_cols))
    return out

class VideoAugmentation():

    def random_noise_v(self,image):
        from skimage.util import random_noise
        image_with_random_noise = random_noise(image)
        return generate_landmark(image_with_random_noise)

    def salt_pepper(self,image):
        img = image
        img_shape = img.shape
        noise = np.random.randint(5, size=img_shape)
        img = np.where(noise == 0, 255, img)
        img = np.where(noise == 0, 0, img)
        return generate_landmark(img)

    def invert(self,image):
        image_with_random_noise = np.invert(image)
        return generate_landmark(image_with_random_noise)

    def random_rotation1(self,image):
        def random_rotation(image_array, random_degree):
            # pick a random degree of rotation between 25% on the left and 25% on the right
            return sk.transform.rotate(image_array, random_degree)
        random_degree = random.uniform(-25, 25)
        image_with_random_noise = random_rotation(image, random_degree)
        return generate_landmark(image_with_random_noise)

    def horizontal_flip(self,image):
        return generate_landmark(image[:, ::-1])

    def vertical_flip(self,image):
        return generate_landmark(image[::-1, :])

def invert(image):
    image_with_random_noise = np.invert(image)
    return image_with_random_noise

def random_rotation1(image):
    def random_rotation(image_array, random_degree):
        # pick a random degree of rotation between 25% on the left and 25% on the right
        return sk.transform.rotate(image_array, random_degree)
    random_degree = random.uniform(-25, 25)
    image_with_random_noise = random_rotation(image,random_degree)
    return image_with_random_noise
def horizontal_flip(image):
    return image[:, ::-1]
def vertical_flip(image):
    return image[::-1, :]

def video_process(input_path):
    one_hand_counter = 0
    two_hand_counter = 0
    two_hand_null_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    video_landmark_output = []
    decoded_frame_count = 0
    max_frame = 60
    temp_landmark_op = []
    hands = mp_hands.Hands(
        min_detection_confidence=0.7, min_tracking_confidence=0.5, max_num_hands=2)
    cap = cv2.VideoCapture(input_path)
    while decoded_frame_count < max_frame:
        if decoded_frame_count == max_frame:
            break
        while cap.isOpened():
            if decoded_frame_count == max_frame:
                break
            success, image = cap.read()
            if not success:
                break
            image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)
            image.flags.writeable = False
            rdm_no1 = VideoAugmentation()
            rdm_no = rdm_no1.vertical_flip(image)
            cv2.imshow('image',rdm_no)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            results = hands.process(image)
            image.flags.writeable = True
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            if results.multi_hand_landmarks:
                num_hands_detected = len(results.multi_hand_landmarks)
                temp_landmark_op = list()
                temp_landmark_op_1 = list()
                if(num_hands_detected == 1):
                    # ONE HAND ONLY
                    # print("One Hand")
                    decoded_frame_count = decoded_frame_count + 1
                    one_hand_counter = one_hand_counter + 1
                    for hand_landmarks in results.multi_hand_landmarks:
                        mp_drawing.draw_landmarks(
                            image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                        temp_landmark_op = decode_landmarks(
                            hand_landmarks.landmark)
                        for y in range(len(two_hand_null_data)):
                            temp_landmark_op.append(two_hand_null_data[y])
                        temp_landmark_op_1 = temp_landmark_op
                    for x in range(len(temp_landmark_op)):
                        video_landmark_output.append(temp_landmark_op[x])
                elif(num_hands_detected == 2):
                    # TWO HAND ONLY
                    # print("Two Hand")
                    decoded_frame_count = decoded_frame_count + 1

                    two_hand_counter = two_hand_counter + 1
                    for hand_landmarks in results.multi_hand_landmarks:
                        mp_drawing.draw_landmarks(
                            image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                        temp_landmark_op = temp_landmark_op + decode_landmarks(
                            hand_landmarks.landmark)
                    temp_landmark_op_1 = temp_landmark_op
                    for x in range(len(temp_landmark_op)):
                        video_landmark_output.append(temp_landmark_op[x])
            # print(len(video_landmark_output))
            # print(len(temp_landmark_op))
            # print(decoded_frame_count)
        if(decoded_frame_count < max_frame):
            while(len(video_landmark_output)!=5040):
                if(len(temp_landmark_op_1)==0):
                    temp_landmark_op_1 = two_hand_null_data
                for z in range(len(temp_landmark_op_1)):
                    video_landmark_output.append(temp_landmark_op_1[z])
                decoded_frame_count = decoded_frame_count + 1
                print(len(video_landmark_output))
    hands.close()
    cap.release()
    # print(decoded_frame_count)
    return(video_landmark_output)

def video_process_blured(input_path):
    one_hand_counter = 0
    two_hand_counter = 0
    two_hand_null_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    video_landmark_output = []
    decoded_frame_count = 0
    max_frame = 60
    temp_landmark_op = []
    hands = mp_hands.Hands(
        min_detection_confidence=0.7, min_tracking_confidence=0.5, max_num_hands=2)
    cap = cv2.VideoCapture(input_path)
    while decoded_frame_count < max_frame:
        if decoded_frame_count == max_frame:
            break
        while cap.isOpened():
            if decoded_frame_count == max_frame:
                break
            success, image = cap.read()
            if not success:
                break
            image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)
            image = cv2.GaussianBlur(image, (35, 35), 0)
            image.flags.writeable = False
            results = hands.process(image)
            image.flags.writeable = True
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            if results.multi_hand_landmarks:
                num_hands_detected = len(results.multi_hand_landmarks)
                temp_landmark_op = list()
                temp_landmark_op_1 = list()
                if(num_hands_detected == 1):
                    # ONE HAND ONLY
                    # print("One Hand")
                    decoded_frame_count = decoded_frame_count + 1
                    one_hand_counter = one_hand_counter + 1
                    for hand_landmarks in results.multi_hand_landmarks:
                        mp_drawing.draw_landmarks(
                            image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                        temp_landmark_op = decode_landmarks(
                            hand_landmarks.landmark)
                        for y in range(len(two_hand_null_data)):
                            temp_landmark_op.append(two_hand_null_data[y])
                        temp_landmark_op_1 = temp_landmark_op
                    for x in range(len(temp_landmark_op)):
                        video_landmark_output.append(temp_landmark_op[x])
                elif(num_hands_detected == 2):
                    # TWO HAND ONLY
                    # print("Two Hand")
                    decoded_frame_count = decoded_frame_count + 1

                    two_hand_counter = two_hand_counter + 1
                    for hand_landmarks in results.multi_hand_landmarks:
                        mp_drawing.draw_landmarks(
                            image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                        temp_landmark_op = temp_landmark_op + decode_landmarks(
                            hand_landmarks.landmark)
                    temp_landmark_op_1 = temp_landmark_op
                    for x in range(len(temp_landmark_op)):
                        video_landmark_output.append(temp_landmark_op[x])
            # print(len(video_landmark_output))
            # print(len(temp_landmark_op))
            # print(decoded_frame_count)
        if(decoded_frame_count < max_frame):
            while(len(video_landmark_output)!=5040):
                if(len(temp_landmark_op_1)==0):
                    temp_landmark_op_1 = two_hand_null_data
                for z in range(len(temp_landmark_op_1)):
                    video_landmark_output.append(temp_landmark_op_1[z])
                decoded_frame_count = decoded_frame_count + 1
                print(len(video_landmark_output))
    hands.close()
    cap.release()
    # print(decoded_frame_count)
    return(video_landmark_output)
    
parser = argparse.ArgumentParser(
    description='Demonstration of landmarks localization.')
parser.add_argument('--from_dir', type=str,
                    help='Path to a directory of videos')
args = parser.parse_args()
if args.from_dir is not None:
    directories = os.listdir(args.from_dir)
    for dir in directories:
        print(directories)
        videos = os.listdir(args.from_dir + "/" + dir)
        print(videos)
        # for index , video in zip(range(8), videos):
        for video in videos:
            input_path = os.path.join(args.from_dir, dir, video)
            # if input_path.endswith(format_tuple):
            test_op = video_process(input_path)
            test_op.append(ges)
            test_op_blured = video_process_blured(input_path)
            test_op_blured.append(ges)
            print("COUNT : ", ges)
            print(len(test_op))
            print(dir)
            print(input_path)
            with open("ges_4_blured.csv", "a") as fp:
                wr = csv.writer(fp)
                wr.writerow(test_op)
                wr.writerow(test_op_blured)
        ges = ges + 1
        info = (test_op[-1], dir ,len(videos))
        print(info)
        info_file.write(str(info))
        info_file.write("\n")
        

info_file.close()