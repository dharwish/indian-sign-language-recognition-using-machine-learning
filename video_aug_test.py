from PIL import Image, ImageSequence

from vidaug import augmentors as va

# sometimes = lambda aug: va.Sometimes(0.5, aug) # Used to apply augmentor with 50% probability
# seq = va.Sequential([
#     va.RandomCrop(size=(240, 180)), # randomly crop video with a size of (240 x 180)
#     va.RandomRotate(degrees=10), # randomly rotates the video with a degree randomly choosen from [-10, 10]  
#     sometimes(va.HorizontalFlip()) # horizontally flip the video with 50% probability
# ])

# for batch_idx in range(1000):
#     # 'video' should be either a list of images from type of numpy array or PIL images
#     video = load_batch(batch_idx)
#     video_aug = seq(video)
#     train_on_video(video)
import cv2
import skvideo.io  
import numpy as np

def video_to_np():
    videodata = skvideo.io.vread("1.mp4")  
    print(videodata.shape)
    video = np.array(videodata) 
    # sometimes = lambda aug: va.Sometimes(0.5, aug) # Used to apply augmentor with 50% probability
    # seq = va.Sequential([
    # va.RandomCrop(size=(240, 180)), # randomly crop video with a size of (240 x 180)
    # va.RandomRotate(degrees=10), # randomly rotates the video with a degree randomly choosen from [-10, 10]  
    # sometimes(va.HorizontalFlip()) # horizontally flip the video with 50% probability
    # ])
    # # for batch_idx in range(1000):
    # # 'video' should be either a list of images from type of numpy array or PIL images
    #     # video = load_batch(batch_idx)
    # video_aug = seq(video)
    # print(video_aug)
    # img = cv2.imread(video_aug,0)
    # cv2.imshow('image',img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    sometimes = lambda aug: va.Sometimes(1, aug) # Used to apply augmentor with 100% probability
    seq = va.Sequential([ # randomly rotates the video with a degree randomly choosen from [-10, 10]  
    sometimes(va.HorizontalFlip()) # horizontally flip the video with 100% probability
    ])
    video_aug = seq(video)

    outputdata = video_aug .random(size=videodata.shape) * 255
    outputdata = outputdata.astype(np.uint8)
    skvideo.io.vwrite("outputvideo.mp4", outputdata)
    return video_aug
video_to_np()
# def gif_loader(path, modality="RGB"):
#     frames = []
#     with open(path, 'rb') as f:
#         with Image.open(f) as video:
#             index = 1
#             for frame in ImageSequence.Iterator(video):
#                 frames.append(frame.convert(modality))
#                 index += 1
#         return frames
# frames = gif_loader("1.mp4")        

    

