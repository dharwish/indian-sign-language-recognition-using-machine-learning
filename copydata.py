import os
import argparse
import subprocess

# example (WARNING: setting `shell=True` might be a security-risk)
# In Linux/Unix
parser = argparse.ArgumentParser(
    description='Demonstration of landmarks localization.')
parser.add_argument('--from_dir', type=str,
                    help='Path to a directory of videos')
args = parser.parse_args()
if args.from_dir is not None:
    directories = os.listdir(args.from_dir)
    for dir in directories:
        print(dir)
        videos = os.listdir(args.from_dir + "/" + dir)
        print(videos)

        # status = subprocess.call('cp' '-r' '' destination.txt', shell=True) 
