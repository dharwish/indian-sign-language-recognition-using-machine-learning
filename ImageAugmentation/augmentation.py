import cv2
import numpy as np
from skimage.transform import PiecewiseAffineTransform, warp
from skimage import data
import random
class VideoAugmentation():

    def __init__(self, *args):
        super().__init__(*args)

    def PiecewiseAffine(self, frame, th1):
        image = frame
        rows, cols = image.shape[0], image.shape[1]

        src_cols = np.linspace(0, cols, 20)
        src_rows = np.linspace(0, rows, 10)
        src_rows, src_cols = np.meshgrid(src_rows, src_cols)
        src = np.dstack([src_cols.flat, src_rows.flat])[0]

        # add sinusoidal oscillation to row coordinates
        dst_rows = src[:, 1] - np.sin(np.linspace(0, 3 * np.pi, src.shape[0])) * 50
        dst_cols = src[:, 0]
        dst_rows *= 1.5
        dst_rows -= 1.5 * 50
        dst = np.vstack([dst_cols, dst_rows]).T


        tform = PiecewiseAffineTransform()
        tform.estimate(src, dst)

        out_rows = image.shape[0] - 1.5 * 50
        out_cols = cols
        out = warp(image, tform, output_shape=(out_rows, out_cols))
        return out

    def GaussianBlur(self, frame, th1, th2):    
        return cv2.GaussianBlur(frame, (th1, th2), 0)

    def InvertColor(self, frame):
        return np.invert(frame)

    def RandomRotation(self, frame, angle):
        angle = int(random.uniform(-angle, angle))
        h, w = frame.shape[:2]
        M = cv2.getRotationMatrix2D((int(w/2), int(h/2)), angle, 1)
        return cv2.warpAffine(frame, M, (w, h))

    def CenterCrop(self, frame, w, h):
        center = tuple(ti/2 for ti in frame.shape)
        x = center[1] - w/2
        y = center[0] - h/2
        return frame[int(y):int(y+h), int(x):int(x+w)]

    def HorizontalFlip(self, frame):
        return frame[:, ::-1]

    def VerticalFlip(self, frame):
        return frame[::-1, :]

    def Salt(self, frame):
        noise = np.random.randint(20, size=frame.shape)
        return np.where(noise == 0, 255, frame)

    def Pepper(self, frame):
        noise = np.random.randint(20, size=frame.shape)
        return np.where(noise == 0, 0, frame)
    
    def SaltAndPepper(self, frame):
        return self.Pepper(self.Salt(frame))
    
    

    