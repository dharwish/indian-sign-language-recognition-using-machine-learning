import csv
import numpy as np
import pandas as pd
import math
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import  MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from keras.layers.core import Dense, Activation, Dropout
from tensorflow.keras.utils import to_categorical
##################################################################################################
#csvr=pd.read_csv('./raman-terabyte/dataset.csv') dataset=np.loadtxt('./raman-terabyte/dataset.csv',delimiter=",",dtype={'names': ('GESTURE 1', 'GESTURE 2', 'GESTURE 3'),'formats': (np.float,
#np.float, np.float, '|S15')},unpack=True) csvr=csv.reader(open('./raman-terabyte/dataset.csv','r'))
target=np.array()
dataset=np.loadtxt('5GESHOLISTIC/ges_4_blured.csv',delimiter=",")
for row in dataset:
    target=np.append()
    
dataset2=dataset(usecols=[x for x in range(0,7080))

dataset2=dataset2.reshape(2544,60,118)

#x=list(csvr)
#dataset=np.array(x)
#dataset=csvr.to_numpy()
print(dataset.shape)
#exit()


#exit()
#target
target=np.array()
# target=np.repeat(target,126)
# Split dataset into training set, test set and validation set
X_train, X_test, y_train, y_test = train_test_split(dataset, target,test_size=0.3,random_state=109)
X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=1)

y_train=to_categorical(y_train)
y_test=to_categorical(y_test)
y_val=to_categorical(y_val)

#print(type(X_test))
#print(X_train.shape)
#print(X_test.shape)
#print(X_val.shape)
#print(dataset.shape)

#X_trainnew=np.empty([50, 35])
#X_testnew=np.empty([27, 35])
#X_valnew=np.empty([13, 35])

#print(X_trainnew.shape)
#for x in X_train:
# np.append(X_trainnew,np.array(np.split(x[:3561530],35)))
#for x in X_test:
#        np.append(X_testnew,np.array(np.split(x[:3561530],35)))
#for x in X_val:
#        np.append(X_valnew,np.array(np.split(x[:3561530],35)))

#print(X_trainnew.shape)
#print(X_testnew.shape)
#print(X_valnew.shape)
#exit()
#################################################################################################

# Build the model
#(35,101758)
print('Build model...')
model = Sequential()
model.add(LSTM(126, return_sequences=False,input_shape=(60,118)))
model.add(Dropout(0.2))
model.add(Dense(units=20))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc'])

batch_size=50
model.fit(X_train, y_train,epochs=500,validation_data=(X_val, y_val))

score, acc = model.evaluate(X_test, y_test, batch_size=batch_size)
print('Test score:', score)
print('Test accuracy:', acc)
