#Importing necessary libraries 
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import metrics
import csv
from sklearn import svm
import tensorflow as tf


dataset=np.loadtxt('ges_4_blured1.csv',delimiter=",")

data = dataset[:,:-1] #all columns except the last one
target = dataset[:,len(dataset[0])-1] #only the last column
X_train, X_test, y_train, y_test = train_test_split(data, target,test_size=0.2,random_state=109)

X_train=tf.keras.utils.normalize(X_train,axis=1)
X_test=tf.keras.utils.normalize(X_test,axis=1)


clf = svm.SVC(kernel="rbf",decision_function_shape='ovo')
clf.fit(X_train,y_train)

y_pred=clf.predict(X_test)

#Model Evaluation
print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
print("Precision:",metrics.precision_score(y_test, y_pred,average="macro"))
print(metrics.precision_recall_fscore_support(y_test, y_pred, average="macro"))
print(metrics.confusion_matrix(y_test, y_pred))
