        landmark_output = []
        mp_drawing = mp.solutions.drawing_utils
        mp_hands = mp.solutions.hands
        hands = mp_hands.Hands(
            min_detection_confidence=0.7, min_tracking_confidence=0.5, max_num_hands=2)
        two_hand_null_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        results = hands.process(image)
        image.flags.writeable = True

        temp_landmark_op = list()
        if results.multi_hand_landmarks:
            num_hands_detected = len(results.multi_hand_landmarks)
            temp_landmark_op_1 = list()
            if(num_hands_detected == 1):            # ONE HAND ONLY
                for hand_landmarks in results.multi_hand_landmarks:
                    mp_drawing.draw_landmarks(
                        image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                    temp_landmark_op = self.decode_landmarks(
                        hand_landmarks.landmark)
                    for y in range(len(two_hand_null_data)):
                        temp_landmark_op.append(two_hand_null_data[y])
                    temp_landmark_op_1 = temp_landmark_op
                for x in range(len(temp_landmark_op)):
                    landmark_output.append(temp_landmark_op[x])
            elif(num_hands_detected == 2):           # TWO HAND ONLY
                for hand_landmarks in results.multi_hand_landmarks:
                    mp_drawing.draw_landmarks(
                        image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                    temp_landmark_op = temp_landmark_op + self.decode_landmarks(
                        hand_landmarks.landmark)
                temp_landmark_op_1 = temp_landmark_op
                for x in range(len(temp_landmark_op)):
                    landmark_output.append(temp_landmark_op[x])