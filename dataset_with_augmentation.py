import argparse
import random
from imutils.video import count_frames
import os
import skimage as sk
import json
import string
import pandas as pd
import mediapipe as mp
import cv2
import time
import sys
import csv
from skimage import transform
from skimage import util
from skimage import data
from skimage import filters
from skimage import color
from skimage import exposure
from skimage.transform import rotate
from skimage.transform import warp
from skimage.transform import ProjectiveTransform
from skimage.transform import AffineTransform
import numpy as np
import skvideo.io
from vidaug import augmentors as va
info_file = open("info_4_blured.txt","w") 
info_file.write("Last Appended Value | Output CSV File | Number Of videos \n") 

def decode_landmarks(landmarks: list, flatten: bool = True) -> dict:
    decoded_output = []
    keys = ['1', '2', '3', '4', '5', '6', '7', '8',
            '9', '10', '11', '12', '13', '14', '15', '16', '17',
            '18', '19', '20', '21']
    for i, key in enumerate(keys):
        data_point = landmarks[i]
        if flatten:
            x = data_point.x
            y = data_point.y
            decoded_output.append(x)
            decoded_output.append(y)
    return(decoded_output)


def generate_landmark(image):
    landmark_output = []
    mp_drawing = mp.solutions.drawing_utils
    mp_hands = mp.solutions.hands
    hands = mp_hands.Hands(
        min_detection_confidence=0.7, min_tracking_confidence=0.5, max_num_hands=2)
    two_hand_null_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    results = hands.process(image)
    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    if results.multi_hand_landmarks:
        num_hands_detected = len(results.multi_hand_landmarks)
        temp_landmark_op = list()
        temp_landmark_op_1 = list()
        if(num_hands_detected == 1):            # ONE HAND ONLY
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                temp_landmark_op = decode_landmarks(
                    hand_landmarks.landmark)
                for y in range(len(two_hand_null_data)):
                    temp_landmark_op.append(two_hand_null_data[y])
                temp_landmark_op_1 = temp_landmark_op
            for x in range(len(temp_landmark_op)):
                landmark_output.append(temp_landmark_op[x])
        elif(num_hands_detected == 2):           # TWO HAND ONLY
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                temp_landmark_op = temp_landmark_op + decode_landmarks(
                    hand_landmarks.landmark)
            temp_landmark_op_1 = temp_landmark_op
            for x in range(len(temp_landmark_op)):
                landmark_output.append(temp_landmark_op[x])
    hands.close()
    return(landmark_output)



image = 0
def video_process(input_path):
    vid_aug = VideoAugmentation()
    video_landmark_output = []
    max_frame = 60
    decoded_frame_count = 0
    cap = cv2.VideoCapture(input_path)
    while decoded_frame_count < max_frame:
        if decoded_frame_count == max_frame:
            break
        while cap.isOpened():
            print(decoded_frame_count)
            if decoded_frame_count == max_frame:
                break
            success, image = cap.read()
            if not success:
                break
            
            decoded_frame_count = decoded_frame_count + 1
            
    cap.release()
    return(video_landmark_output)

parser = argparse.ArgumentParser(
    description='Demonstration of landmarks localization.')
parser.add_argument('--from_dir', type=str,
                    help='Path to a directory of videos')
args = parser.parse_args()
if args.from_dir is not None:
    directories = os.listdir(args.from_dir)
    for dir in directories:
        print(directories)
        videos = os.listdir(args.from_dir + "/" + dir)
        print(videos)
        # for index , video in zip(range(8), videos):
        for video in videos:
            input_path = os.path.join(args.from_dir, dir, video)
            # if input_path.endswith(format_tuple):
            test_op = video_process(input_path)
            print(len(test_op))
            with open("AugGesOutput.csv", "a") as fp:
                wr = csv.writer(fp)
                wr.writerow(test_op)
        info = (test_op[-1], dir ,len(videos))
        info_file.write(str(info))
        info_file.write("\n")
info_file.close()
