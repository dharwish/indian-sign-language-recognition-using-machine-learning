# Indian Sign Language Recognition Using Machine Learning 
## Prerequisites
* Python (Im using 3.7)
## Setup Env
* ###  Install ```virtualenv``` for creating a Virtual Environment  
```shel 
sudo apt-get install virtualenv

virtualenv isl_env --python=python3.7

source isl_env/bin/activate
```
* ### Install Required Packages
<!-- Packages:
* Mediapipe
* Numpy
* Tensorflow (For training)
* Scikit-learn
* pandas
* scipy
* skvideo -->
```shell
pip install -r requirements.txt
```
* Random Noise 
* Inverted image
* Random Rotation
* Horizontal Flip
* Vertical Flip
* Pepper (add black points)  Salt (add white points)
* Gaussian Blur
* Affine 
* Piecewise_affine

#skimage.transform.swirl(image[, center, …])
Perform a swirl transformation.


Output
1:(Ges(x) (aug(1)video(n) ) (gesnumber(x)))

20 degree
ra


INCLUDE50
Not found : Red